package com.example.actorMS.infrastructure

import jakarta.persistence.*
import java.time.LocalDate

@Entity
@Table(name = "actor")
class ActorEntity(
    @Id
    @Column(name = "id", nullable = false)
    var id: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Convert(disableConversion = true)
    @Column(name = "birth_date", nullable = false)
    var birthDate: LocalDate,
)