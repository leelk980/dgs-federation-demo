package com.example.actorMS

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ActorMsApplication

fun main(args: Array<String>) {
    runApplication<ActorMsApplication>(*args)
    println("Actor ms application is running")
}
