package com.example.movieMS

import com.example.common.CommonConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import

@SpringBootApplication
@Import(CommonConfig::class)
class MovieMsApplication

fun main(args: Array<String>) {
    runApplication<MovieMsApplication>(*args)
    println("Movie ms application is running")
}
