package com.example.movieMS.infrastructure

import jakarta.persistence.*

@Entity
@Table(name = "movie_character")
class MovieCharacterEntity(
    @Id
    @Column(name = "id", nullable = false)
    var id: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "actor_id", nullable = false)
    var actorId: String,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "movie_id", nullable = false)
    var movie: MovieEntity,
)