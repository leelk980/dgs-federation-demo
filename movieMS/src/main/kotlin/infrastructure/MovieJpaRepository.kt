package com.example.movieMS.infrastructure

import com.example.movieMS.domain.Movie
import com.example.movieMS.domain.MovieRepository
import com.querydsl.jpa.impl.JPAQueryFactory
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional
class MovieJpaRepository(
    private val jpaQueryFactory: JPAQueryFactory,
) : MovieRepository {
    private val qMovie = QMovieEntity.movieEntity

    override fun findOneById(id: String) = runCatching {
        jpaQueryFactory
            .selectFrom(qMovie)
            .where(qMovie.id.eq(id))
            .fetchOne()
            ?.let {
                convertToDomainObject(it)
            }
            ?: throw Exception("not found exception")
    }

    override fun findManyMovie() = runCatching {
        jpaQueryFactory
            .selectFrom(qMovie)
            .fetch()
            .let {
                it.map { each -> convertToDomainObject(each) }
            }
    }

    private fun convertToDomainObject(entity: MovieEntity) = entity.run {
        Movie(
            id = id,
            title = title,
            publishedAt = publishedAt,
            movieCharacters = entity.movieCharacters.map {
                Movie.Character(
                    id = it.id,
                    name = it.name,
                    actorId = it.actorId
                )
            }
        )
    }
}