package com.example.movieMS.infrastructure

import jakarta.persistence.*
import java.time.LocalDateTime

@Entity
@Table(name = "movie")
class MovieEntity(
    @Id
    @Column(name = "id", nullable = false)
    var id: String,

    @Column(name = "title", nullable = false)
    var title: String,

    @Convert(disableConversion = true)
    @Column(name = "published_at", nullable = false)
    var publishedAt: LocalDateTime,

    @OneToMany(mappedBy = "movie")
    var movieCharacters: MutableSet<MovieCharacterEntity>
)