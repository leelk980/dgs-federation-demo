package com.example.movieMS.interfaces

import com.example.movieMS.domain.Movie
import com.example.movieMS.domain.MovieRepository
import com.example.movieMS.generated.dgs.types.*
import com.netflix.graphql.dgs.DgsComponent
import com.netflix.graphql.dgs.DgsQuery
import com.netflix.graphql.dgs.InputArgument

@DgsComponent
class MovieQuery(
    private val movieRepository: MovieRepository,
) {
    @DgsQuery
    fun findOneMovieById(@InputArgument("id") id: String): FindOneMovieByIdView {
        return movieRepository
            .findOneById(id)
            .mapCatching {
                FindOneMovieByIdView(
                    movie = convertDomainObjectToField(it)
                )
            }
            .getOrThrow()
    }

    @DgsQuery
    fun findManyMovies(@InputArgument("pagination") pagination: PaginationInput): FindManyMoviesView {
        return movieRepository
            .findManyMovie()
            .mapCatching {
                FindManyMoviesView(
                    data = it.map { each -> convertDomainObjectToField(each) },
                    pagination = PaginationField(
                        totalElement = 0,
                        totalPage = 0,
                        index = 0,
                        size = 0
                    )
                )
            }
            .getOrThrow()
    }

    private fun convertDomainObjectToField(domainObject: Movie): MovieField {
        return domainObject.run {
            MovieField(
                id = id,
                title = title,
                publishedAt = publishedAt,
                movieCharacters = movieCharacters.map {
                    MovieCharacterField(
                        id = it.id,
                        name = it.name,
                        actorId = it.actorId
                    )
                }
            )
        }
    }
}