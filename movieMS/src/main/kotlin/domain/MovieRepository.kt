package com.example.movieMS.domain

interface MovieRepository {
    fun findOneById(id: String): Result<Movie>

    fun findManyMovie(): Result<List<Movie>>
}