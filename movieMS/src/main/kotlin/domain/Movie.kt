package com.example.movieMS.domain

import java.time.LocalDateTime

data class Movie(
    var id: String,
    var title: String,
    var publishedAt: LocalDateTime,
    var movieCharacters: List<Character>,
) {
    data class Character(
        var id: String,
        var name: String,
        var actorId: String,
    )
}