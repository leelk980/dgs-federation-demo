import {ApolloServer} from '@apollo/server';
import {startStandaloneServer} from '@apollo/server/standalone';
import {ApolloGateway} from '@apollo/gateway';
import {readFileSync} from 'fs';

const gateway = new ApolloGateway({supergraphSdl: readFileSync('./supergraph.graphql').toString()});

const server = new ApolloServer({gateway});

const {url} = await startStandaloneServer(server, {listen: {port: 8000}});

console.log(`🚀  Server ready at ${url}`);