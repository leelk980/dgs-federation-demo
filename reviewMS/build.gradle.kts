val msName = "reviewMS"

allOpen {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.Embeddable")
    annotation("jakarta.persistence.MappedSuperclass")
}

noArg {
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.Embeddable")
    annotation("jakarta.persistence.MappedSuperclass")
}

plugins {
    id("com.netflix.dgs.codegen") version "5.6.6"
}

dependencies {
    implementation(project(":common"))

    // dgs
    implementation(platform("com.netflix.graphql.dgs:graphql-dgs-platform-dependencies:latest.release"))
    implementation("com.netflix.graphql.dgs:graphql-dgs-spring-boot-starter")

    // queryDsl
    implementation("com.vladmihalcea:hibernate-types-60:2.20.0")
    implementation("com.infobip:infobip-spring-data-jpa-querydsl-boot-starter:8.0.0")
    kapt("com.querydsl:querydsl-apt:5.0.0:jakarta")
}

tasks.generateJava {
    schemaPaths = mutableListOf("common/src/main/resources/schema", "${projectDir}/src/main/resources/schema")
    packageName = "com.example.$msName.generated.dgs"
    generateClient = true
    snakeCaseConstantNames = true
    typeMapping = mutableMapOf(
        "UtcTime" to "java.time.LocalDateTime",
    )
}