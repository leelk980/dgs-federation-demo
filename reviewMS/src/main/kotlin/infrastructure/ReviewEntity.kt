package com.example.reviewMS.infrastructure

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "review")
class ReviewEntity(
    @Id
    @Column(name = "id", nullable = false)
    var id: String,

    @Column(name = "content", nullable = false)
    var content: String,

    @Column(name = "movie_id", nullable = false)
    var movieId: String,
)