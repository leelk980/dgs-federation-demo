package com.example.reviewMS.infrastructure

import com.example.reviewMS.domain.Review
import com.example.reviewMS.domain.ReviewRepository
import com.querydsl.jpa.impl.JPAQueryFactory
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional


@Repository
@Transactional
class ReviewJpaRepository(
    private val jpaQueryFactory: JPAQueryFactory,
) : ReviewRepository {
    private val qReview = QReviewEntity.reviewEntity

    override fun findManyByMovieIds(movieIds: List<String>) = runCatching {
        jpaQueryFactory
            .selectFrom(qReview)
            .where(qReview.movieId.`in`(movieIds))
            .fetch()
            .let {
                it.map { each -> convertToDomainObject(each) }
            }
    }

    private fun convertToDomainObject(entity: ReviewEntity) = entity.run {
        Review(
            id = id,
            content = content,
            movieId = movieId,
        )
    }
}