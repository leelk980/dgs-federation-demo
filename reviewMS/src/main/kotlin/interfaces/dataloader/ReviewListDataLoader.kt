package com.example.reviewMS.interfaces.dataloader

import com.example.common.extension.extFillEmptyList
import com.example.reviewMS.domain.ReviewRepository
import com.example.reviewMS.generated.dgs.types.ReviewField
import com.netflix.graphql.dgs.DgsDataLoader
import org.dataloader.MappedBatchLoader
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage

@DgsDataLoader
class ReviewListDataLoader(
    private val reviewRepository: ReviewRepository,
) : MappedBatchLoader<String, List<ReviewField>> {
    override fun load(movieIds: Set<String>): CompletionStage<Map<String, List<ReviewField>>> {
        return CompletableFuture.supplyAsync {
            reviewRepository
                .findManyByMovieIds(movieIds.toList())
                .mapCatching {
                    it.groupBy(
                        { each -> each.movieId },
                        { each ->
                            ReviewField(
                                id = each.id,
                                content = each.content,
                            )
                        }
                    ).extFillEmptyList(movieIds)
                }
                .getOrThrow()
        }
    }
}