package com.example.reviewMS.interfaces

import com.example.reviewMS.generated.dgs.DgsConstants
import com.example.reviewMS.generated.dgs.types.MovieField
import com.example.reviewMS.generated.dgs.types.ReviewField
import com.example.reviewMS.interfaces.dataloader.ReviewListDataLoader
import com.netflix.graphql.dgs.DgsComponent
import com.netflix.graphql.dgs.DgsData
import com.netflix.graphql.dgs.DgsDataFetchingEnvironment
import com.netflix.graphql.dgs.DgsEntityFetcher
import java.util.concurrent.CompletableFuture

@DgsComponent
class ReviewExtend {
    @DgsEntityFetcher(name = DgsConstants.MOVIE_FIELD.TYPE_NAME)
    fun movie(values: Map<String, String>): MovieField {
        return MovieField(id = values["id"]!!, reviews = listOf())
    }

    @DgsData(parentType = DgsConstants.MOVIE_FIELD.TYPE_NAME)
    fun reviews(dfe: DgsDataFetchingEnvironment): CompletableFuture<List<ReviewField>> {
        val source = dfe.getSource<MovieField>()
        val dataLoader = dfe.getDataLoader<String, List<ReviewField>>(ReviewListDataLoader::class.java)

        return dataLoader.load(source.id)
    }
}