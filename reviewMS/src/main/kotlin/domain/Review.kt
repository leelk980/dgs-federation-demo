package com.example.reviewMS.domain

data class Review(
    var id: String,
    var content: String,
    var movieId: String,
)