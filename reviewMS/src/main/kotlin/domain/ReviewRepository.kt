package com.example.reviewMS.domain


interface ReviewRepository {
    fun findManyByMovieIds(movieIds: List<String>): Result<List<Review>>
}