package com.example.reviewMS

import com.example.common.CommonConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import

@SpringBootApplication
@Import(CommonConfig::class)
class ReviewMsApplication

fun main(args: Array<String>) {
    runApplication<ReviewMsApplication>(*args)
    println("Review ms application is running")
}
