package com.example.common.extension


/**
 * @description
 * - groupBy하고 없는 key에 대해 empty List로 채워줌
 * */
fun <K : Any, V> Map<K, List<V>>.extFillEmptyList(keys: Collection<K>): Map<K, List<V>> {
    return keys.fold(mutableMapOf()) { acc, each ->
        acc[each] = this.getOrElse(each) { listOf() }
        acc
    }
}
